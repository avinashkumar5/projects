package com.interview;

/*
n = 12 in three parts {12/2, 12/3, 12/4}
then {6,4,3} = 13 , futher 6 is divided as {3,2,1} = 6 so max is 6 only
futher 4 is divided as {2,1,0} = 3, but even breaking the max is still the original which is 4 hence taken 4
then 3 is divided as {1,0,0} = 1 but again 3 is greater hence taken 3 only*/
public class MaxDemonetisation {

	public static void main(String[] args) {

		int token = 12; 
		System.out.println("Max Indian Rupee :::" + getMaxCurrency(token));
		
	}
	
	public static int getMaxCurrency(int token) {
		
		if (token == 0 || token == 1)
	    	return token;
		
		return Math.max((getMaxCurrency(token/2) + getMaxCurrency(token/3) + getMaxCurrency(token/4)), token);
	}

}
