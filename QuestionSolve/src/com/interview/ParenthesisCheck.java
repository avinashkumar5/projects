package com.interview;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class ParenthesisCheck {
	
	public static void main(String args[]) {
		
		List<String> words = Arrays.asList("[d(d(k))d]","[[[[]]]]", "{ac[bb]}", "{3234[fd");
		words.forEach(word -> {
			System.out.println("Word " + word + " is correctly closed " + validateParenthesis(word));	
		});
	
	}
	
	
	public static boolean validateParenthesis(String word) {
		
		Stack<Character> stack = new Stack<>();
		for(char c : word.toCharArray()) {
			if ( c == '{' || c == '[' || c == '(') {
				stack.push(c);
			}
			if (stack.isEmpty()) {
				return false;
			}
			
			char last = stack.peek();
			if ((c == '}' && last == '{') || (c == ']' && last == '[') || (c == ')' && last == '(')) {
				stack.pop();
			}
		}
				
		return stack.isEmpty();
	}

}
